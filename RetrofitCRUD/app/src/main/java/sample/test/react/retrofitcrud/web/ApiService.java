package sample.test.react.retrofitcrud.web;



import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public interface ApiService {
    //http://192.168.2.17/edisposisi/android/cek_login.php?user=feetbo90&pass=feetbo90
    @GET("/tgd/insert.php")
    void setInsert(@Query("user") String username,
                  @Query("pass") String password,
                  Callback<String> callback);

    @FormUrlEncoded
    @POST("/tgd/insert.php")
    void setInsertPost(@Field("user") String username,
                   @Field("pass") String password,
                   Callback<String> callback);
}
