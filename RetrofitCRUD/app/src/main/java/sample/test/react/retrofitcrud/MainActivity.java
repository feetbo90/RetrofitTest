package sample.test.react.retrofitcrud;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import sample.test.react.retrofitcrud.web.ApiService;
import sample.test.react.retrofitcrud.web.RestClient;

public class MainActivity extends AppCompatActivity {

    protected OkApplication application;
    private ApiService api ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText username = (EditText)findViewById(R.id.username);
        final EditText password = (EditText)findViewById(R.id.password);
        Button btn = (Button) findViewById(R.id.tekan);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kirimKeServer(username.getText().toString(), password.getText().toString());
            }
        });


    }

    public void kirimKeServer(String username , String password)
    {
        api = RestClient.getService();
        api.setInsertPost(username, password,callback);

    }

    private Callback<String> callback = new Callback<String>() {
        @Override
        public void success(String s, Response response) {
            if(s.equals("1"))
            {
                Toast.makeText(MainActivity.this, "Sukses", Toast.LENGTH_LONG).show();
            }

        }

        @Override
        public void failure(RetrofitError error) {

            Toast.makeText(MainActivity.this, "Gagal", Toast.LENGTH_LONG).show();
        }
    };

}
