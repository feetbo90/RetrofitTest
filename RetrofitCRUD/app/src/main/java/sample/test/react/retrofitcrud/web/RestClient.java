package sample.test.react.retrofitcrud.web;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.android.AndroidLog;
import retrofit.client.OkClient;

public class RestClient {
    private static final String API_URL = "http://192.168.2.17";

    public static ApiService getService() {
        // Set timeout ke 20 second
        OkHttpClient client = new OkHttpClient();
        client.setConnectTimeout(20, TimeUnit.SECONDS);
        client.setReadTimeout(20, TimeUnit.SECONDS);

        // Set Gson sebagai converter
        return new RestAdapter.Builder()
                .setEndpoint(API_URL)
                .setClient(new OkClient(client))
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .setLog(new AndroidLog("WEB-SERVICE"))
                .build()
                .create(ApiService.class);
    }
}
