package sample.test.react.retrofitcrud;

import android.app.Application;

import sample.test.react.retrofitcrud.web.ApiService;
import sample.test.react.retrofitcrud.web.RestClient;

/**
 * Created by root on 24/02/17.
 */

public class OkApplication extends Application {
    private ApiService apiService;

    public ApiService getApi() {
        if (apiService == null) apiService = RestClient.getService();
        return apiService;
    }
}
